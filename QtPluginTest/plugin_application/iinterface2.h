#ifndef IINTERFACE2_H
#define IINTERFACE2_H

#include <qdebug.h>

#ifndef __FUNC_SIG__
#define __FUNC_SIG__ __PRETTY_FUNCTION__
#endif

class IInterface2
{
protected:

public:
	virtual ~IInterface2() { qDebug() << __FUNC_SIG__; }

	virtual void doSomething() const = 0;
	virtual int doSomethingElse(const int value) const = 0;

	virtual void aFunctionSpecificToIInterface2() const = 0;
};

//	These macros are needed to allow the plugins to be built into libraries
#define IINTERFACE2_IID "plugin.test.iinteface2"
Q_DECLARE_INTERFACE(IInterface2, IINTERFACE2_IID)

#endif // IINTERFACE2_H
