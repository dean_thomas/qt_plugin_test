#ifndef ITOOLBARPROVIDER_H
#define ITOOLBARPROVIDER_H

#include <QList>
#include <QDebug>

#ifndef __FUNCSIG__
#define __FUNCSIG__ __PRETTY_FUNCTION__
#endif

class QToolBar;

class IToolBarProvider
{
public:
	using QToolBarList = QList<QToolBar*>;

	virtual QToolBarList getToolBars() const = 0;

	virtual ~IToolBarProvider() { qDebug() << __FUNCSIG__; }
};

//	These macros are needed to allow the plugins to be built into libraries
#define ITOOLBARPROVIDER_IID "IToolBarProvider"
Q_DECLARE_INTERFACE(IToolBarProvider, ITOOLBARPROVIDER_IID)

#endif // ITOOLBARPROVIDER_H
