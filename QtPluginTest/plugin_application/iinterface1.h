#ifndef IINTERFACE1_H
#define IINTERFACE1_H

#include <qdebug.h>

#ifndef __FUNCSIG__
#define __FUNCSIG__ __PRETTY_FUNCTION__
#endif

class IInterface1
{
protected:
	int m_state;
public:
	virtual ~IInterface1() { qDebug() << __FUNCSIG__; }

	virtual void doSomething() const = 0;
	virtual int doSomethingElse(const int value) const = 0;

	virtual void setState(const int state) = 0;
	virtual int getState() const = 0;
};

//	These macros are needed to allow the plugins to be built into libraries
#define IINTERFACE1_IID "plugin.test.iinteface1"
Q_DECLARE_INTERFACE(IInterface1, IINTERFACE1_IID)

#endif // IINTERFACE1_H
