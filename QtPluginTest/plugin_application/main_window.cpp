#include "main_window.h"
#include "ui_main_window.h"
#include <QPluginLoader>
#include <QDir>
#include <qdebug.h>
#include <iinterface1.h>
#include <iinterface2.h>
#include <IMenuProvider.h>
#include <IToolBarProvider.h>
#include <iostream>

using namespace std;

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	//	Keep track of plugins we were able to load
	QStringList pluginFileNames;

	//	Directory where we are to look for our RUN TIME plugins
	QDir pluginsDir("./plugins/");
	for (auto& fileName : pluginsDir.entryList(QDir::Files))
	{
		//	Try to load each file as a plugin
		QPluginLoader loader(pluginsDir.absoluteFilePath(fileName));
		QObject *plugin = loader.instance();
		if (plugin != nullptr)
		{
			pluginFileNames += fileName;
			m_plugins += plugin;

			auto iMenuPlugin = qobject_cast<IMenuProvider*>(plugin);
			if (iMenuPlugin != nullptr)
			{
				auto myMenus = iMenuPlugin->getMainMenus();
				qDebug() << "Widget provides " << myMenus.size()
						 << " menus";
				for (auto& menu : myMenus)
				{
					ui->menuBar->addMenu(menu);
				}
			}

			auto iToolBarPlugin = qobject_cast<IToolBarProvider*>(plugin);
			if (iToolBarPlugin != nullptr)
			{
				auto myToolBars = iToolBarPlugin->getToolBars();
				qDebug() << "Widget provides " << myToolBars.size()
						 << " tool bars";
				for (auto& toolBar : myToolBars)
				{
					addToolBar(toolBar);
				}
			}
		}
	}

	qDebug() << "Loaded the following plugins: " << pluginFileNames;
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::on_pushButton_clicked()
{
	qDebug() << "Testing cast to IInterface1";
	for (auto& plugin: m_plugins)
	{
		auto iTestPlugin = qobject_cast<IInterface1*>(plugin);
		if (iTestPlugin != nullptr)
		{
			iTestPlugin->doSomething();
		}
	}
	qDebug() << endl;
	qDebug() << "Testing cast to IInterface2";
	for (auto& plugin: m_plugins)
	{
		auto iTestPlugin = qobject_cast<IInterface2*>(plugin);
		if (iTestPlugin != nullptr)
		{
			iTestPlugin->doSomething();
			iTestPlugin->aFunctionSpecificToIInterface2();
		}
	}
	qDebug() << endl;
}
