#ifndef IMENUPROVIDER_H
#define IMENUPROVIDER_H

#include <QList>
#include <QDebug>

#ifndef __FUNCSIG__
#define __FUNCSIG__ __PRETTY_FUNCTION__
#endif

class QMenu;
//class QToolBar;
//class QAction;

//	This interface allows the defintion of two virtual functions.
//	1.	getMainMenu() provides the chance to construct a menu that will be
//		permanently visible when the widgit is loaded.
//	2.	getContextMenu() provides a custom context menu.
//
//	Returning a nullptr to either of these will indicate the menu is not
//	implemented.
class IMenuProvider
{
public:
	using QMenuList = QList<QMenu*>;

	virtual QMenuList getMainMenus() const = 0;
	virtual QMenu* getContextMenu() const = 0;

	virtual ~IMenuProvider() { }
};

//	These macros are needed to allow the plugins to be built into libraries
#define IMENUPROVIDER_IID "IMenuProvider"
Q_DECLARE_INTERFACE(IMenuProvider, IMENUPROVIDER_IID)

#endif // IMENUPROVIDER_H
