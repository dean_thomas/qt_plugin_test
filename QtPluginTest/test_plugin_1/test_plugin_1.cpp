#include "test_plugin_1.h"
#include <iostream>
#include <QMenu>
#include <QToolBar>
#include <QMessageBox>

using namespace std;

TestPlugin1::TestPlugin1(QObject *parent)
	: QObject(parent)
{
	qDebug() << this << " : " << __FUNCSIG__;

	auto f_buildMenu1 = [&]()
	{
		auto action1 = new QAction("Action &1", this);
		connect(action1, &QAction::triggered,
				[&]()
		{
			qDebug() << "1 clicked";
		});

		auto action2 = new QAction("Action &2", this);
		connect(action2, &QAction::triggered,
				[&]()
		{
			qDebug() << "2 clicked";
		});

		//	Create a menu
		auto menu1 = new QMenu("&Menu 1");
		menu1->addActions({ action1, action2 });

		//	Create a toolbar
		auto toolBar1 = new QToolBar("ToolBar 1");
		toolBar1->addAction(action1);
		toolBar1->addAction(action2);

		m_mainMenus.push_back(menu1);
		m_toolBars.push_back(toolBar1);

		auto action3 = new QAction("Action &3", this);
		connect(action3, &QAction::triggered,
				[&]()
		{
			qDebug() << "3 clicked";
		});
		action3->setCheckable(true);

		auto action4 = new QAction("Action &4", this);
		connect(action4, &QAction::triggered,
				[&]()
		{
			qDebug() << "4 clicked";
		});

		//	Create a menu
		auto menu2 = new QMenu("&Menu 2");
		menu2->addActions({ action3, action4 });

		//	Create a toolbar
		auto toolBar2 = new QToolBar("ToolBar 2");
		toolBar2->addAction(action3);
		toolBar2->addAction(action4);

		m_mainMenus.push_back(menu2);
		m_toolBars.push_back(toolBar2);
	};

	f_buildMenu1();
}

void TestPlugin1::doSomething() const
{
	qDebug() << this << " : " << __FUNCSIG__;
}

int TestPlugin1::doSomethingElse(const int) const
{
	qDebug() << this << " : " << __FUNCSIG__;
	return 1;
}

void TestPlugin1::setState(const int)
{
	qDebug() << this << " : " << __FUNCSIG__;
}

int TestPlugin1::getState() const
{
	qDebug() << this << " : " << __FUNCSIG__;
	return m_state;
}

IMenuProvider::QMenuList TestPlugin1::getMainMenus() const
{
	return m_mainMenus;
}

QMenu *TestPlugin1::getContextMenu() const
{
	return nullptr;
}

IToolBarProvider::QToolBarList TestPlugin1::getToolBars() const
{
	return m_toolBars;
}
