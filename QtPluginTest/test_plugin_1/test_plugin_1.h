#ifndef TEST_PLUGIN_1_H
#define TEST_PLUGIN_1_H

#include <QObject>
#include <iinterface1.h>
#include <IMenuProvider.h>
#include <IToolBarProvider.h>

//class QToolBar;

//	This class implements IInterface1
class TestPlugin1 :
		public QObject,
		public IInterface1,
		public IMenuProvider,
		public IToolBarProvider
{
	Q_OBJECT
	//	We can call  the Q_PLUGIN_METADATA and Q_INTERFACES macros
	//	for each implemented interface as follows...
	Q_PLUGIN_METADATA(IID "plugin.test.iinterface1")
	Q_PLUGIN_METADATA(IID "IMenuProvider")
	Q_PLUGIN_METADATA(IID "IToolBarProvider")
	Q_INTERFACES(IInterface1)
	Q_INTERFACES(IMenuProvider)
	Q_INTERFACES(IToolBarProvider)
public:
	explicit TestPlugin1(QObject *parent = 0);

	virtual void doSomething() const override;
	virtual int doSomethingElse(const int) const override;

	virtual void setState(const int) override;
	virtual int getState() const override;

	virtual ~TestPlugin1() override { qDebug() << __FUNCSIG__; }

	virtual QMenuList getMainMenus() const override;
	virtual QMenu* getContextMenu() const override;

	virtual QToolBarList getToolBars() const override;
protected:
	QMenuList m_mainMenus;

	QToolBarList m_toolBars;
signals:

public slots:
};

#endif // TEST_PLUGIN_1_H
