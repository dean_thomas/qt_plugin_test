#ifndef TEST_PLUGIN_3_H
#define TEST_PLUGIN_3_H

#include <QObject>
#include "iinterface1.h"
#include "iinterface2.h"
#include "IToolBarProvider.h"

//	This class implements the IInterface1 and IInterface2
class TestPlugin3 :
		public QObject,
		public IInterface1,
		public IInterface2,
		public IToolBarProvider
{
	Q_OBJECT
	//	We can call  the Q_PLUGIN_METADATA and Q_INTERFACES macros
	//	for each implemented interface as follows...
	Q_PLUGIN_METADATA(IID "plugin.test.iinterface1")
	Q_PLUGIN_METADATA(IID "plugin.test.iinterface2")
	Q_PLUGIN_METADATA(IID "plugin.test.IToolBarProvider")
	Q_INTERFACES(IInterface1)
	Q_INTERFACES(IInterface2)
	Q_INTERFACES(IToolBarProvider)
public:
	explicit TestPlugin3(QObject *parent = nullptr);

	virtual void doSomething() const override;
	virtual int doSomethingElse(const int) const override;

	virtual void setState(const int)  override;
	virtual int getState() const override;

	virtual void aFunctionSpecificToIInterface2() const override;

	virtual ~TestPlugin3() override { qDebug() << __FUNC_SIG__; }

	virtual QToolBarList getToolBars() const override;
signals:

public slots:

protected:
	QToolBarList m_toolBars;
};

#endif // TEST_PLUGIN_3_H
