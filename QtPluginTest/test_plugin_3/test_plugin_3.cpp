#include "test_plugin_3.h"

#include <qdebug.h>
#include <QtWidgets>

#ifndef __FUNC_SIG__
#define __FUNC_SIG__ __PRETTY_FUNCTION__
#endif

TestPlugin3::TestPlugin3(QObject *parent) : QObject(parent)
{
	qDebug() << this << " : " << __FUNC_SIG__;

	auto f_toolBar1 = [&]()
	{
		auto labelWidth = new QLabel("Width");
		auto comboWidth = new QComboBox();
		comboWidth->addItems({ "1", "2", "3", "4", "5" });

		auto labelHeight = new QLabel("Height");
		auto comboHeight = new QComboBox();
		comboHeight->addItems({ "1", "2", "3", "4", "5" });

		auto toolBar1 = new QToolBar("Size");
		toolBar1->addWidget(labelWidth);
		toolBar1->addWidget(comboWidth);
		toolBar1->addSeparator();
		toolBar1->addWidget(labelHeight);
		toolBar1->addWidget(comboHeight);

		auto toolBar2 = new QToolBar("Date");
		toolBar2->addWidget(new QDateTimeEdit(QDate()));

		m_toolBars.push_back(toolBar1);
		m_toolBars.push_back(toolBar2);
	};

	f_toolBar1();
}

void TestPlugin3::doSomething() const
{
	qDebug() << this << " : " << __FUNC_SIG__;
}

int TestPlugin3::doSomethingElse(const int) const
{
	qDebug() << this << " : " << __FUNC_SIG__;
	return 0;
}

void TestPlugin3::setState(const int)
{
	qDebug() << this << " : " << __FUNC_SIG__;
}

int TestPlugin3::getState() const
{
	qDebug() << this << " : " << __FUNC_SIG__;
	return m_state;
}

void TestPlugin3::aFunctionSpecificToIInterface2() const
{
	qDebug() << this << " : " << __FUNC_SIG__;
}

IToolBarProvider::QToolBarList TestPlugin3::getToolBars() const
{
	return m_toolBars;
}


