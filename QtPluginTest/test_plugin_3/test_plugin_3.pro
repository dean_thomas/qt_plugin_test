QT += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = lib

INCLUDEPATH +=	../plugin_application

HEADERS += \
    test_plugin_3.h

SOURCES += \
    test_plugin_3.cpp

DESTDIR += ../plugin_application/plugins
