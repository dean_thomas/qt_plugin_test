#ifndef TEST_PLUGIN_2_H
#define TEST_PLUGIN_2_H

#include <QObject>
#include "iinterface2.h"
#include "IMenuProvider.h"

//	This class implements IInterface2
class TestPlugin2 : public QObject, public IInterface2, public IMenuProvider
{
	Q_OBJECT
	//	We can call  the Q_PLUGIN_METADATA and Q_INTERFACES macros
	//	for each implemented interface as follows...
	Q_PLUGIN_METADATA(IID "plugin.test.iinterface2")
	Q_PLUGIN_METADATA(IID "IMenuProvider")
	Q_INTERFACES(IInterface2)
	Q_INTERFACES(IMenuProvider)
public:
	explicit TestPlugin2(QObject *parent = nullptr);

	virtual void doSomething() const override;
	virtual int doSomethingElse(const int) const override;

	virtual void aFunctionSpecificToIInterface2() const override;

	virtual ~TestPlugin2() override { qDebug() << __FUNC_SIG__; }

	virtual QMenuList getMainMenus() const override;
	virtual QMenu* getContextMenu() const override { return nullptr; }
signals:

public slots:

protected:
	QMenuList m_mainMenus;
};

#endif // TEST_PLUGIN_2_H
