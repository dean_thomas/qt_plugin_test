#include "test_plugin_2.h"
#include <QAction>
#include <QMenu>
#include <QToolBar>

TestPlugin2::TestPlugin2(QObject *parent)
	: QObject(parent)
{
	auto f_buildMenu1 = [&]()
	{
		auto actionA = new QAction("Action &A", this);
		connect(actionA, &QAction::triggered,
				[&]()
		{
			qDebug() << "A clicked";
		});

		auto actionB = new QAction("Action &B", this);
		connect(actionB, &QAction::triggered,
				[&]()
		{
			qDebug() << "B clicked";
		});

		auto actionC = new QAction("Action &C", this);
		connect(actionC, &QAction::triggered,
				[&]()
		{
			qDebug() << "C clicked";
		});

		//	Create a menu
		auto menuA = new QMenu("&Menu A");
		menuA->addActions({ actionA, actionB, actionC });

		m_mainMenus.push_back(menuA);
	};

	f_buildMenu1();

	qDebug() << this << " : " << __FUNC_SIG__;
}

void TestPlugin2::doSomething() const
{
	qDebug() << this << " : " << __FUNC_SIG__;
}

int TestPlugin2::doSomethingElse(const int) const
{
	qDebug() << this << " : " << __FUNC_SIG__;
	return 2;
}

void TestPlugin2::aFunctionSpecificToIInterface2() const
{
	qDebug() << this << " : " << __FUNC_SIG__;
}

IMenuProvider::QMenuList TestPlugin2::getMainMenus() const
{
	return m_mainMenus;
}
